import styled from "styled-components";

export const Place = styled.input`
  text-align: center;
  font-size: 32px;
  background-color: transparent;
  border: 0;
  outline: none;
  margin: 10px 0;
  color: #fed426;
  text-shadow: 0px 1px 2px rgba(0, 0, 0, 0.8);
  width: 100%;
  padding: 0;

  &:focus {
    outline: none;
  }
`;
